class DonVisController < ApplicationController
    def index
      # @don_vis = DonVi.all.order(:stt)
      @don_vis = DonVi.where(parent_code: nil)
    end

    def show
      @don_vi = DonVi.find(params[:id])
    end

    private

    def set_don_vi
      @don_vi = DonVi.find(params[:id])
    end
end
