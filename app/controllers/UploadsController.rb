class UploadsController < ApplicationController
  def new
    @duong_dan = nil

    if params[:don_vi].present?
      don_vi = DonVi.find_by_code(params[:don_vi])
      @don_vi = don_vi
      all_don_vi = don_vi.all_ancestors.pluck(:code)
      all_don_vi = all_don_vi.reverse 
      all_don_vi << don_vi.code
      @duong_dan = all_don_vi.join('/') if all_don_vi.present?
    end
  end

  def create
    s3 = Aws::S3::Resource.new(
      access_key_id: ENV['MIN_USER'],
      secret_access_key: ENV['MIN_PASSWORD'],
      endpoint: ENV['MIN_ENDPOINT'],
      force_path_style: true,
      region: "us-east-1"
    )
    bucket_name = params[:ten_bucket].present? ? params[:ten_bucket] : 'buck1'
    duong_dan = params[:duong_dan]
    file = params[:file]

    bucket = s3.bucket(bucket_name)

    file_path = file.original_filename
    file_path = File.join(duong_dan, file.original_filename) if duong_dan.present?

    # Kiểm tra xem bucket đã tồn tại chưa
    unless bucket.exists?
      bucket = s3.create_bucket(bucket: bucket_name)
    end

    obj = bucket.object(file_path)
    obj.upload_file(file.tempfile)

    p 'File uploaded successfully'
    redirect_to new_upload_path(don_vi: params[:don_vi].to_s), notice: 'File uploaded successfully'
  rescue StandardError => e
    flash[:alert] = "Error uploading file: #{e.message}"
    p "Error uploading file: #{e.message}"
    redirect_to new_upload_path(don_vi: params[:don_vi].to_s)
  end
end
