class DonVi < ApplicationRecord
    # Khai báo các trường
    self.table_name = "donvis"
  
    # Định nghĩa các trường
    # stt (integer)
    # ten (string)
    # cap (integer)
    # dia_chi (string)
    # dien_thoai (string)
    # ma_dinh_danh_dt (string)
    # code (string)
    # parent_code (string)
  
    # Thuộc tính liên quan
    # Quan hệ cha con với chính nó
    has_many :don_vi_con, class_name: "DonVi", primary_key: "code", foreign_key: "parent_code", dependent: :destroy
    belongs_to :don_vi_cha, class_name: "DonVi", primary_key: "code", foreign_key: "parent_code", optional: true
  
    # Validations
    # ...
    def has_children?
      self.don_vi_con.any?
    end
  
    def all_ancestors
      ancestors = []
      current_don_vi = self.don_vi_cha
  
      while current_don_vi.present?
        ancestors << current_don_vi
        current_don_vi = current_don_vi.don_vi_cha
      end
  
      ancestors
    end

  end
  