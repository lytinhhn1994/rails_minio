class CreateDonVis < ActiveRecord::Migration[7.1]
  def change
    create_table :donvis do |t|
      t.string :stt
      t.string :ten
      t.integer :cap
      t.string :dia_chi
      t.string :dien_thoai
      t.string :ma_dinh_danh_dt
      t.string :code
      t.string :parent_code
    end

    add_index :donvis, :code
    add_index :donvis, :parent_code
  end
end
