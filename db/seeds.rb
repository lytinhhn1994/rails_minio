# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

# DonVi.delete_all

data = [
  {
    stt: "0",
    ten: "UBND tỉnh Bắc Ninh",
    cap: 1,
    dia_chi: "",
    dien_thoai: "",
    ma_dinh_danh_dt: "H05",
    code: "H05",
    parent_code: nil,
  },
  {
    stt: "1",
    ten: "Ban An toàn giao thông tỉnh Bắc Ninh",
    cap: 2,
    dia_chi: "Số 16 đường Nguyên Phi Ỷ Lan, phường Suối Hoa, thành phố Bắc Ninh, tỉnh Bắc Ninh",
    dien_thoai: "2223827352",
    ma_dinh_danh_dt: "H05.1",
    code: "H05.1",
    parent_code: "H05",
  },
  {
    stt: "1.1",
    ten: "Văn phòng Ban",
    cap: 3,
    dia_chi: "",
    dien_thoai: "2373845678",
    ma_dinh_danh_dt: "H02.44.02",
    code: "H05.1.1",
    parent_code: "H05.1",
  },
  {
    stt: "2",
    ten: "Ban quản lý an toàn thực phẩm",
    cap: 2,
    dia_chi: "",
    dien_thoai: "",
    ma_dinh_danh_dt: "H05.2",
    code: "H05.2",
    parent_code: "H05",
  },
]

DonVi.create!(data)
